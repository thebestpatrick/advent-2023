use std::fs;

// ¯\_(ツ)_/¯

#[derive(Debug)]
struct SourceToDestData {
    source_start: u64,
    dest_start: u64,
    range_length: u64,
}

impl SourceToDestData {
    fn in_range(&self, n: u64) -> bool {
        let source_end = self.source_start + self.range_length - 1;
        if n < source_end && n >= self.source_start {
            true
        }
        else {
            false
        }
    }

    fn translate_num(&self, n: u64) -> u64 {
        if !self.in_range(n) {
            return n;
        }
        
        let d: u64 = n - self.source_start;
        let r: u64 = self.dest_start + d;
        println!("    - {}:", n);
        println!("      line: {} {} {}", self.dest_start, self.source_start, self.range_length);
        println!("      new: {}", r);
        return r;
    }
}

fn text_to_nums(text: &str) -> Vec<u64> {
    text.split(" ").filter_map(|c| c.parse::<u64>().ok()).collect::<Vec<u64>>()
}

fn get_seed_ids(line: &str) -> Vec<u64> {
    let l = line.replace("seeds: ", "");
    
    text_to_nums(&l)
}

fn get_map_vals(line: &str) -> SourceToDestData {
    let mut map_nums = vec![0; 3];
    let raw_nums = text_to_nums(line);
    SourceToDestData {
        dest_start: raw_nums[0],
        source_start: raw_nums[1],
        range_length: raw_nums[2],
    }
}

fn build_map_section(alm_chunk: &str) -> Vec<SourceToDestData> {
    let mut alm_sec = alm_chunk
        .split("\n")
        .filter(|v| !v.is_empty());
    
    // skip the `name-to-name map:` line since they are in order
    alm_sec.next();

    alm_sec.map(|m| get_map_vals(m)).collect()
}

fn translate_id(seed_id: u64, map_secs: &Vec<Vec<SourceToDestData>>) -> u64 {
    let mut s: u64 = seed_id;
    for (i, ms) in map_secs.iter().enumerate() {
        println!("  map {}: ", i+1);
        for m in ms {
            s = m.translate_num(s);
        }
    }
    s
}

fn main() {
    let alm_data = fs::read_to_string("almanac.txt")
        .expect("Unable to read almanac.txt");

    let mut alm = alm_data
        .split("\n\n")
        .filter(|v| !v.is_empty());

    let seed_ids = get_seed_ids(alm.next().unwrap());
    let map_secs: Vec<Vec<SourceToDestData>> = alm.map(|s| build_map_section(s)).collect();
    let mut final_store: Vec<u64> = Vec::new();
    for sid in seed_ids {
        println!("\n---\nseed id {:?}:", sid);
        let s = translate_id(sid, &map_secs);
        final_store.push(s);
        println!("  val: {}", s);
    }
    for f in &final_store {
        println!("# {}", f);
    }
    println!("\n\n#final = {:?}", final_store.iter().min());
}