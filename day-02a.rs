use std::fs;
use std::collections::HashMap;

// Day 1 part 1 saving here for overhauls to part 2

const BLOCK_COLORS: &[(&str, usize)] = &[
    ("red", 12),
    ("green", 13),
    ("blue", 14),
];

fn check_score(color: String, score: usize) -> bool {
    // Checks if the given color and score are possible.
    for e in BLOCK_COLORS.into_iter() {
        if e.0 == color {
            if score > e.1 {
                return false;
            } else {
                return true;
            }
        }
    }
    println!("Warning: Could not find matching key for color '{}'", color);
    return false;
}

fn check_scores(scorecard: HashMap<String, usize>) -> bool {
    for (key, value) in &scorecard {
        if !check_score(key.to_string(), *value) {
            return false;
        }
    }
    return true;
}

fn get_game_id(gl: &str) -> usize {
    let gid: usize = gl[5..].parse().unwrap();
    return gid;
}

fn color_round(sec: &str) -> (String, usize) {
    let mut color_name_c: String = String::new();
    let mut num_c: String = String::new();

    for c in sec.chars() {
        if c.is_digit(10) {
            num_c.push(c);
        }
        else {
            if c != ' ' {
                color_name_c.push(c);
            }
        }
    }
    return (color_name_c.clone(), num_c.parse().unwrap());
}

fn process_game(line: &str) -> usize {
    // yes, exactly two (2) please. thank you.
    let gbreak: Vec<&str> = line.splitn(2, ':').collect();

    if gbreak.len() != 2 {
        println!("Split line into {} components: {:?}", gbreak.len(), gbreak);
        return 0;
    }
    else {
        let game_id = get_game_id(gbreak[0]);
        println!("game {}:", game_id);
        let rounds: Vec<&str> = gbreak[1].split(';').collect();

        let mut round_num = 0;
        for round in rounds.into_iter() {
            let mut game_scores: HashMap<String, usize> = HashMap::new();
            round_num += 1;
            let rlow = round.to_lowercase();
            //let round_score = process_round(&rlow);
            let block_counts_r: Vec<&str> = rlow.split(',').collect();
            for block_count_r in block_counts_r.into_iter() {
                let cr = color_round(block_count_r);
                let count = game_scores.entry(cr.0).or_insert(0);
                *count += cr.1;
            }
            let game_possible = check_scores(game_scores.clone());
            if !game_possible {
                println!("\timpossible: {:?}", game_scores);
                return 0;
            }
        }

        return game_id;
    }
}

fn process_games(data: String) -> usize {
    let mut t: usize = 0;
    for l in data.lines() {
        let game_score = process_game(&l);
        println!("");
        t += game_score;
    }
    return t;
}

fn main() {
    let game_data = fs::read_to_string("games.txt")
        .expect("Unable to read games.txt");

    let total_ids = process_games(game_data);
    println!("Total score of {}", total_ids);
}
