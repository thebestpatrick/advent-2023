fn text_to_nums(text: &str) -> Vec<i32> {
    let mut n: Vec<i32> = Vec::new();

    let nums = text.split(" ");
    for a in nums {
        if a != "" {
            let v: i32 = a.trim().parse().unwrap();
            n.push(v);
        }
    }
    n 
}

fn process_card(line: &str) -> i32 {
    let base: i32 = 2;
    let card_num: i32 = line[4..8].trim().parse().unwrap();

    let lotto_nums  = line[10..40].trim();
    let win_nums    = line[42..].trim();

    let ln = text_to_nums(lotto_nums);

    let wn = text_to_nums(win_nums);

    base.pow((ln.iter().filter(|n| wn.contains(n)).count()).try_into().unwrap()) / 2
}