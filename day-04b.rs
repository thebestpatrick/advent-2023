fn text_to_nums(text: &str) -> Vec<i32> {
    let mut n: Vec<i32> = Vec::new();

    let nums = text.split(" ");
    for a in nums {
        if a != "" {
            let v: i32 = a.trim().parse().unwrap();
            n.push(v);
        }
    }
    n 
}

fn score_card(ln: Vec<i32>, wn: Vec<i32>) -> i32 {
    (ln.iter().filter(|n| wn.contains(n)).count()).try_into().unwrap()
}

fn process_card(line: &str) -> i32 {
    let lotto_nums  = line[10..40].trim();
    let win_nums    = line[42..].trim();

    let ln = text_to_nums(lotto_nums);

    let wn = text_to_nums(win_nums);

    score_card(ln, wn)
}

fn solitaire_win(card_data: &str) -> i32 {
    let mut cards: Vec<i32> = Vec::new();
    for l in card_data.lines() {
        cards.push(process_card(l));
    }

    let card_count = cards.len();

    let mut copies: Vec<i32> = Vec::with_capacity(card_count);
    for _i in 0..card_count {
        copies.push(1);
    }

    for (ci, real_card) in cards.iter().enumerate() {
        let s: usize = ci + 1;
        let e: usize = *real_card as usize + s;
        let a: i32 = copies[ci];
        for _ii in s..e {
            if _ii < card_count {
                copies[_ii] += a;
            }
            else {
                break;
            }
        }
    }
    copies.iter().sum()
}