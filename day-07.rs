use std::fs;
use std::collections::HashMap;
use std::cmp::Ordering;

static FACEVALS: &str = "TJQKA";

static FIVEKD: usize = 500;
static FOURKD: usize = 400;
static THREKD: usize = 350;
static FULLHS: usize = 300;
static TWOPAR: usize = 200;
static ONEPAR: usize = 100;
static HICARD: usize = 1;

#[derive(Debug)]
#[derive(Clone)]
struct CamelCards<'a> {
    hand_s: &'a str,
    bid: usize,
}

fn rough_score(h: &str) -> HashMap<char, usize> {
    let mut hm = HashMap::new();
    for c in h.chars() {
        let card_src = hm.entry(c).or_insert(0);
        *card_src += 1;
    }
    hm
}

fn face_val(card: char) -> usize {
    if card.is_numeric() {
        return card.to_string().parse().unwrap();
    }
    let fv = FACEVALS.find(card);
    if fv.is_some() {
        return fv.unwrap() + 10;
    }
    println!("face val err");
    0
}

impl CamelCards <'_> {
    pub fn score(&self) -> usize {
        let hm: HashMap<char, usize> = rough_score(self.hand_s);
        let hml: usize = hm.len();

        let kwm = hm.iter().max_by_key(|entry | entry.1).unwrap();

        match hml {
            1 => FIVEKD,
            2 => {
                // 4 of a kind or full house
                if kwm.1 == &4 {
                    return FOURKD;
                }
                FULLHS
            }
            3 => {
                // 3 of a kind, 1 or 2 pair
                if kwm.1 == &3 {
                    return THREKD;
                }
                if kwm.1 == &2 {
                    for (k, v) in hm.clone().into_iter() {
                        if k == *kwm.0 {
                            continue
                        }
                        if v == 2 {
                            return TWOPAR;
                        }
                    }
                }
                return ONEPAR;
            }
            4 => ONEPAR,
            _ => HICARD,
        }
    }

    pub fn spec_cmp(&self, other_h: CamelCards) -> Option<Ordering> {
        let this_s = self.score();
        let that_s = other_h.score();
        if this_s == that_s {
            // complicated
            return self.tie_break(other_h);
        }
        if this_s > that_s {
            return Some(Ordering::Greater);
        }
        Some(Ordering::Less)
    }

    pub fn tie_break(&self, c2: CamelCards) -> Option<Ordering> {
        for i in 0..5 as usize {
            let ca = face_val(self.hand_s.as_bytes()[i] as char);
            let cb = face_val(c2.hand_s.as_bytes()[i] as char);
            if ca > cb {
                return Some(Ordering::Greater);
            }
            if ca < cb {
                return Some(Ordering::Less);
            }
        }
        println!("Completely equal hands: {:?} = {:?}", self, c2);
        Some(Ordering::Equal)
    }
}

fn create_hand(hand_s: &str, bid: usize) -> CamelCards {
    CamelCards{ hand_s, bid }
}

fn true_dat(line: &str) -> (&str, usize) {
    let line_parts: Vec<&str> = line.split(" ").collect();
    (line_parts[0], line_parts[1].parse().unwrap())
}

fn main() {
    let hands_data = fs::read_to_string("hands.txt")
        .expect("Unable to read hands.txt");

    let mut v: Vec<CamelCards> = hands_data.lines()
        .map(|l| true_dat(&l))
        .map(|(hand, bid)| create_hand(hand, bid))
        .collect();

    v.sort_by(|a, b| a.spec_cmp(b.clone()).unwrap());

    let mut bid_scores: Vec<usize> = v.iter()
        .enumerate()
        .map(|(rank, cc_hand)| {
            //println!("{} * {}", rank, cc_hand.bid);
            (rank + 1 as usize) * cc_hand.bid
        }
        )
        .collect();
    println!("{}", bid_scores.into_iter().sum::<usize>());
    // (ノ-_-)ノ ミ ┴┴
}