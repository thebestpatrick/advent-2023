use std::fs;

const NUMBERNAMES: &[&str] = &["zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"];

fn process_line(line: &str) -> usize {
    let mut s: Vec<char> = Vec::new();
    for c in line.chars() {
        if c.is_digit(10) {
            s.push(c);
        }
    }
    let l = s.len();
    if l > 0 {
        let char2pos = l - 1;

        // Theoretically this should have error handling
        let r: usize = format!("{}{}", s[0], s[char2pos])
            .parse()
            .unwrap();

        return r;
    }
    else {
        // This should maybe alert, I don't **think** it should ever happen
        return 0;
    }
}

fn replace_num_names(line: String) -> String {
    let mut pos = 0;
    let mut new_line: String = line.clone();
    for n in NUMBERNAMES.into_iter() {
        let (start, end) = n.split_at(2);
        let pos_c = format!("{}{}{}", start, pos, end);
        let ns = format!("{}", n);
        //println!("{}: {}", pos_c, ns);
        new_line = new_line.replace(&ns, &pos_c);
        //println!("{}", new_line);
        pos += 1;
    }
    return new_line;
}

fn process_lines(data: String) -> usize {
    let mut t: usize = 0;
    for l in data.lines() {
        let r = process_line(l);
        t += r;
    }
    return t;
}

fn main() {
    let cal_data = fs::read_to_string("calibrations.txt")
        .expect("Unable to read calibrations.txt");

    println!("calibration len:\t{}", cal_data.len());
    let proc_data = replace_num_names(cal_data.clone());
    println!("processed len:\t{}", proc_data.len());
    let t1 = process_lines(cal_data);
    let t2 = process_lines(proc_data);

    println!("final results: pt1={}, pt2={}", t1, t2)
}
